uukuuk
======

[Logo-like language][1] for apes.

[ ![Codeship Status for fleg/uukuuk](https://codeship.com/projects/66a4ecd0-21e7-0133-1126-52bb0fef976f/status?branch=master)](https://codeship.com/projects/96014)

Language description
--------------------

Example program:

    Eeeeek:
        Uuuuuuuk!
        UuuuUUUUUUuuk. # explanation of what's going on for non-apes
        OoooOoooOoOoOOoooooooooooooooooOOOoook?
    Ek.

* Drawing instruction consists of three parts:
    * Length definition
        * You need to use `u`, uppercase or lowercase, for length definition,
        * As much of these letters you'll use, as long the created line will be,
        * If you'll use `o` instead of `u` you will move the pen without drawing
    * `k` marks the end of length definition,
    * Direction in which the line should be drawn:
        * `!` - Turn left
        * `?` - Turn right
        * `.` - Go straight
* Loop instruction is similar to length definitions, but `E` is used instead of `U`.
    * `[Ee]*k:` marks the beginning of the loop (notice the `:`), while `Ek.` marks the end,
    * Everything between the beginning and the end of the loop will be done exactly _X_ times,
    * Where _X_ is the number of `E` in the loop beginning instruction.
* Newlines, spaces and tabulations are ignored
* `#` ignores the rest of the line - _useful for comments!_

[1]: https://en.wikipedia.org/wiki/Logo_%28programming_language%29 "Logo programming language"
