require 'rubygems'
require 'bundler/setup'

require 'green_shoes'
require 'uri'
require "./lang/uuklang.rb"
require "./lang/commands.rb"
require "./ui/definitions.rb"
require "./lang/definitions.rb"

@margin = 20
@flow_width = (Ui::Definitions::WindowSize::WIDTH-2*@margin)
@flow_heigth = (Ui::Definitions::WindowSize::HEIGHT-4*@margin)

Treetop.load "lang/uuk"
include Ui::Definitions
include Uuk::Commands
include Uuk::Definitions::Direction

class UukGui < Shoes
  url '/', :index
  url '/render/(.*)', :render

  def index
    stack(:margin => @margin) do
      @word = edit_box
      button "OK" do
        @str = URI.escape @word.text
        visit "/render/#{@str}"
      end
    end
  end
  def render(string)
    @str = URI.unescape(string)
    parser = UukParser.new
    parse_result = parser.parse(@str)
    doc = Uuk::Document.new
    execution_result = doc.execute(parse_result)
    stack(width: WindowSize::WIDTH, height: WindowSize::HEIGHT) do
      stack(top: 50) do
        button "back" do
          visit "/"
        end
      end
      stack do
        position = {}
        position[:x] = (WindowSize::WIDTH/2-PIXEL_SIZE/2).to_i
        position[:y] = (WindowSize::WIDTH/2-PIXEL_SIZE/2).to_i
        execution_result.each do |command|
          # should we draw?
          if command.instance_of? Draw
            rect position[:x], position[:y], PIXEL_SIZE, PIXEL_SIZE
          end
          # move the pen
          if (NORTH === command.direction) then position[:y]=position[:y]+PIXEL_SIZE end
          if (EAST  === command.direction) then position[:x]=position[:x]+PIXEL_SIZE end
          if (SOUTH === command.direction) then position[:y]=position[:y]-PIXEL_SIZE end
          if (WEST  === command.direction) then position[:x]=position[:x]-PIXEL_SIZE end
        end
      end
    end
  end
end
