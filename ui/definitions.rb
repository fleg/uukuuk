module Ui
  module Definitions
    WINDOW_TITLE = "Uuk"
    PIXEL_SIZE = 4
    module WindowSize
        WIDTH  = 800
        HEIGHT = 600
    end
  end
end
