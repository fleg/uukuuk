module Uuk
  module Definitions
    module TurnDirection
      LEFT_TURN  = -1
      NO_TURN    =  0
      RIGHT_TURN =  1
    end
    module Direction
      NORTH = 0
      EAST  = 1
      SOUTH = 2
      WEST  = 3
      MAX_DIRECTION = 4
    end
  end
end
