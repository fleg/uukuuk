require "./lang/definitions.rb"

include Uuk::Definitions::TurnDirection

module Uuk
  module Utils
    def self.turnToDirection(previousDirection, turn)
      return (previousDirection+turn)%MAX_DIRECTION
    end

    def self.langToDirection(s)
      case s
      when '!'
        return LEFT_TURN
      when '?'
        return RIGHT_TURN
      when '.'
        return NO_TURN
      end
    end
  end
end
