module Uuk
  module Commands
    class Command; end

    class MovementCommand < Command
      include Comparable
      def <=>(other)
        if not self.is_a? other.class
          return nil
        end
        @direction <=> other.direction
      end

      def initialize(direction)
        @direction = direction
      end

      def direction
        return @direction
      end
    end

    class MovePen < MovementCommand; end
    class Draw < MovementCommand; end
  end
end
