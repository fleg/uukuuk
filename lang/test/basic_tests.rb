require 'rubygems'
require 'bundler/setup'

require "test/unit"
require 'polyglot'
require 'treetop'
require "./lang/uuklang.rb"
require "./lang/commands.rb"
require "./lang/definitions.rb"

include Uuk::Commands
include Uuk::Definitions::Direction

class BasicLowLevelParserTests < Test::Unit::TestCase
  def test_basic_parsing
    Treetop.load "lang/uuk"

    parser = UukParser.new
    assert(parser.parse(File.open("lang/test.uuk").read))
  end

  def test_basic_executing
    Treetop.load "lang/uuk"

    parser = UukParser.new
    result = parser.parse(File.open("lang/test.uuk").read)
    assert result
    doc = Uuk::Document.new
    assert doc.execute result
  end

  def test_draw_one
    Treetop.load "lang/uuk"

    expected_result = [Draw.new(EAST)]

    parser = UukParser.new
    result = parser.parse("Uk.")
    assert result
    doc = Uuk::Document.new
    assert_equal(expected_result, doc.execute(result))
  end

  def test_move_one
    Treetop.load "lang/uuk"

    expected_result = [MovePen.new(EAST)]

    parser = UukParser.new
    result = parser.parse("Ok.")
    assert result
    doc = Uuk::Document.new
    assert_equal(expected_result, doc.execute(result))
  end

  def test_draw_move_one
    Treetop.load "lang/uuk"

    expected_result = [Draw.new(EAST), MovePen.new(EAST)]

    parser = UukParser.new
    result = parser.parse("Uk.Ok.")
    assert result
    doc = Uuk::Document.new
    assert_equal(expected_result, doc.execute(result))
  end

  def test_draw_move_turning_left
    Treetop.load "lang/uuk"

    expected_result = [Draw.new(NORTH), MovePen.new(WEST)]

    parser = UukParser.new
    result = parser.parse("Uk!Ok!")
    assert result
    doc = Uuk::Document.new
    assert_equal(expected_result, doc.execute(result))
  end

  def test_draw_move_turning_right
    Treetop.load "lang/uuk"

    expected_result = [Draw.new(SOUTH), MovePen.new(WEST)]

    parser = UukParser.new
    result = parser.parse("Uk?Ok?")
    assert result
    doc = Uuk::Document.new
    assert_equal(expected_result, doc.execute(result))
  end

  def test_draw_x2_move_x2_turning_right
    Treetop.load "lang/uuk"

    expected_result = [Draw.new(SOUTH), Draw.new(SOUTH), MovePen.new(WEST), MovePen.new(WEST)]

    parser = UukParser.new
    result = parser.parse("Uuk?Ook?")
    assert result
    doc = Uuk::Document.new
    assert_equal(expected_result, doc.execute(result))
  end

  def test_draw_x2_move_x2_turning_left
    Treetop.load "lang/uuk"

    expected_result = [Draw.new(NORTH), Draw.new(NORTH), MovePen.new(WEST), MovePen.new(WEST)]

    parser = UukParser.new
    result = parser.parse("Uuk!Ook!")
    assert result
    doc = Uuk::Document.new
    assert_equal(expected_result, doc.execute(result))
  end

  def test_draw_in_a_loop
    Treetop.load "lang/uuk"

    expected_result = [Draw.new(NORTH), Draw.new(WEST)]

    parser = UukParser.new
    result = parser.parse("Eek:Uk!Ek.")
    assert result
    doc = Uuk::Document.new
    assert_equal(expected_result, doc.execute(result))
  end

  def test_simple_program
    Treetop.load "lang/uuk"

    expected_result = [
      Draw.new(NORTH),
      Draw.new(NORTH),
      MovePen.new(EAST),
      MovePen.new(EAST),
      MovePen.new(EAST),
      Draw.new(SOUTH),
      Draw.new(EAST),
      Draw.new(EAST),
      MovePen.new(SOUTH),
      MovePen.new(SOUTH),
      MovePen.new(SOUTH),
      Draw.new(WEST)
    ]

    parser = UukParser.new
    result = parser.parse("Eek: Uuk! Oook? Uk? Ek.")
    assert result
    doc = Uuk::Document.new
    assert_equal(expected_result, doc.execute(result))
  end
end
