require 'rubygems'
require 'bundler/setup'

require "test/unit"
require "./lang/definitions.rb"
require "./lang/utils.rb"

include Uuk::Definitions::Direction
include Uuk::Definitions::TurnDirection

class TurnToDirection < Test::Unit::TestCase
  def test_no_turn_doesnt_change_direction
    assert_equal(NORTH, Uuk::Utils::turnToDirection(NORTH, NO_TURN))
    assert_equal(EAST , Uuk::Utils::turnToDirection(EAST , NO_TURN))
    assert_equal(SOUTH, Uuk::Utils::turnToDirection(SOUTH, NO_TURN))
    assert_equal(WEST , Uuk::Utils::turnToDirection(WEST , NO_TURN))
  end

  def test_turning_left
    assert_equal(NORTH, Uuk::Utils::turnToDirection(EAST,  LEFT_TURN))
    assert_equal(WEST,  Uuk::Utils::turnToDirection(NORTH, LEFT_TURN))
    assert_equal(SOUTH, Uuk::Utils::turnToDirection(WEST,  LEFT_TURN))
    assert_equal(EAST,  Uuk::Utils::turnToDirection(SOUTH, LEFT_TURN))
  end

  def test_turning_right
    assert_equal(EAST,  Uuk::Utils::turnToDirection(NORTH, RIGHT_TURN))
    assert_equal(NORTH, Uuk::Utils::turnToDirection(WEST,  RIGHT_TURN))
    assert_equal(WEST,  Uuk::Utils::turnToDirection(SOUTH, RIGHT_TURN))
    assert_equal(SOUTH, Uuk::Utils::turnToDirection(EAST,  RIGHT_TURN))
  end
end
