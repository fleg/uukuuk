require 'treetop'
require "./lang/definitions.rb"
require "./lang/commands.rb"
require "./lang/utils.rb"

include Uuk::Definitions::Direction
include Uuk::Commands

module Uuk
  class Document
    def initialize
      @command_queue = []
    end

    def execute(element)
      if element.is_a? Uuk::Instruction
        element.execute self
      elsif element.is_a? Uuk::Comment
        # silently ignore
      elsif element.nonterminal?
        element.elements.each {|e| execute e }
      end
      return @command_queue
    end

    def previous_direction
      return EAST if @command_queue.empty? # default direction at start
      return @command_queue.last.direction
    end

    def push(c)
      @command_queue.push c
    end
  end

  class Comment < Treetop::Runtime::SyntaxNode
  end

  class Space < Treetop::Runtime::SyntaxNode
  end

  class LoopDeclaration < Treetop::Runtime::SyntaxNode
    def count
      return count_decl.text_value.length
    end
  end

  class DrawLineDeclaration < LoopDeclaration
    def direction
      return direction_decl.text_value
    end
  end

  class PenMoveDeclaration < DrawLineDeclaration
  end

  class Instruction < Treetop::Runtime::SyntaxNode
  end

  class DrawLine < Instruction
    def count
      return elements.select{|x| x.is_a? DrawLineDeclaration}.first.count
    end
    def direction
      return Uuk::Utils::langToDirection(elements.select{|x| x.is_a? DrawLineDeclaration}.first.direction)
    end
    def execute(doc)
      dir = Uuk::Utils::turnToDirection(doc.previous_direction, direction)
      (1..(count)).each { doc.push(Draw.new(dir)) }
    end
  end

  class PenMove < Instruction
    def count
      return elements.select{|x| x.is_a? PenMoveDeclaration}.first.count
    end
    def direction
      return Uuk::Utils::langToDirection(elements.select{|x| x.is_a? PenMoveDeclaration}.first.direction)
    end
    def execute(doc)
      dir = Uuk::Utils::turnToDirection(doc.previous_direction, direction)
      (1..(count)).each { doc.push(MovePen.new(dir)) }
    end
  end

  class Loop < Instruction
    def count
      return elements.select{|x| x.is_a? LoopDeclaration}.first.count
    end
    def execute(doc)
      (1..count).each { elements.each {|e| doc.execute e} }
    end
  end

  class Block < Treetop::Runtime::SyntaxNode
  end

  class Statement < Treetop::Runtime::SyntaxNode
  end
end
