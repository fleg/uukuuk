require 'rubygems'
require 'bundler/setup'

require 'polyglot'
require 'treetop'
require './lang/uuklang.rb'

Treetop.load "lang/uuk"

parser = UukParser.new
result = parser.parse(File.open("test.uuk").read)
if result
  doc = Uuk::Document.new
  doc.execute result
else
  puts parser.failure_reason
  puts parser.failure_line
  puts parser.failure_column
end
