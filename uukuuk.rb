require "./ui/ui.rb"
require "./ui/definitions.rb"

include Ui::Definitions
Shoes.app(title: WINDOW_TITLE, width: WindowSize::WIDTH, height: WindowSize::HEIGHT, resizable: false)
